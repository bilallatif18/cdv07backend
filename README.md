# CDV07Backend
This is a backend project with Node JS serving as a backend server and the database being used is MongoDB.


# Start the Server
Run npm run start for a dev server. Navigate to http://localhost:7777/CDV/sm/{API_PATH} to call any API.


# Pre-Requisites
1) MongoDB should up in order to start the Backend server.
2) Automated python script in /Datascripts needs to be executed in order to call backend APIs to insert data in the database.

