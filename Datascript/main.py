import glob, os
import requests


stock_json = []
url = "http://localhost:7777/CDV/sm/stock"
os.chdir("/Users/bilallatif/Saarland/Media Informatics/MAD/The_digitised_collections_Saarlandmuseum")
for file in glob.glob("*.jpg"):
    img_dict = {
        "name": file,
        "title": "",
        "artist": "",
        "year": 0,
        "description": "",
        "tags": []
    }
    stock_json.append(img_dict)

for stock in stock_json:
    print(stock)
    res = requests.post(url, json=stock)
    print(res.text)


