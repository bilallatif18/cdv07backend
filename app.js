const express = require('express');
const http = require('http');
const mongoose = require('mongoose');
const app = express();
const dotenv = require('dotenv').config({path: __dirname + '/launchSettings.env'});
const router = express.Router();
const UserRoutes =  require('./routes/User');
const StockRoutes =  require('./routes/Stock');
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(bodyParser.json({limit: "50mb"}));
app.use(cors());

//MongoDB connection
mongoose.connect(process.env.DATABASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('error', function(error){
    console.log('Error Connecting Database');
    process.exit(1);
});

db.once('open', function(){
    console.log('DB connection alive');
});

//Routes for our APIs
router.use(function(req, res, next) {
    console.log('Request to /CDV/sm');
    next();
  });
UserRoutes(router);
StockRoutes(router);

//Registering our Routes
app.use('/CDV/sm',  router);

const server=  http.createServer(app);

server.listen(process.env.PORT);
console.log(`Server is listening on ${process.env.PORT}`);