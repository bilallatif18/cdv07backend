const mongoose = require('mongoose');

const UpdateInfoSchema = new mongoose.Schema({
    date: Date,
    delay: Number,
    key_delay: Number,
    before_incentive: Boolean,
    session_id: String,
});

module.exports = mongoose.model("UpdateInfo", UpdateInfoSchema);