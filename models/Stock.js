const mongoose = require("mongoose");
const TagSchema = require('../models/Tag');

const StockSchema = new mongoose.Schema(
    {
        name: String,
        title: String,
        artist: String,
        year: Number,
        description: String,
        tags: [TagSchema.schema],
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Stock",StockSchema);