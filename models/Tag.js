const mongoose = require('mongoose');
const UpdateInfoSchema = require('../models/UpdateInfo');

const TagSchema = new mongoose.Schema({
    label: String,
    count: Number,
    updateInfo: [UpdateInfoSchema.schema]
});

module.exports = mongoose.model("Tag", TagSchema);