const StockController = require('../controllers/StockController');

function StockRoutes(router){
    router
    .route('/stock')
    .post(StockController.create_update_stock)
    .get(StockController.getAllStocks);
    router
        .route('/stock/:stock_id')
        .get(StockController.getStockById);
    router
        .route('/tag/:tags')
        .get(StockController.findByTags);
}
module.exports = StockRoutes;