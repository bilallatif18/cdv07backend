const UserController = require('../controllers/UserController');

function UserRoutes(router){
    router
        .route('/User')
        .get(UserController.getUsers);
}
module.exports = UserRoutes;