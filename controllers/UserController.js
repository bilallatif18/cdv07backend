
const User = require('../models/User');

exports.getUsers = function(req, res, next) {
    User.find({})
        .then(user => {
            res.json(user);
        })
        .catch(next);
};