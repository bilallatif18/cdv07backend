const Stock = require('../models/Stock');

exports.create_update_stock = function(req, res, next) {
    const stock = new Stock(req.body);
    let isNew = true;
    if(req.body._id){
        stock.isNew = false;
        isNew  = false;
    }
    stock.save()
        .then(stock => {
            if(isNew) {
                res.json("Data Inserted in stocks");
            }else {
                res.json("Stock with id: "+req.body._id+" has been updated");
            }
        })
        .catch(next);
};

exports.getAllStocks = function(req, res, next) {
    Stock.find({})
        .then(stock => {
            res.json(stock);
        })
        .catch(next);
};

exports.getStockById = function(req, res, next) {
    Stock.findById(req.params.stock_id)
        .then(stock => {
            res.json(stock);
        })
        .catch(next);
};

exports.findByTags = function(req, res, next) {
    let tags = req.params.tags.split(',');
    Stock.find({"tags.label": { $in: tags} })
        .then(stock => {
            res.json(stock);
        })
        .catch(next);
};